package uclouvain.mssm.test

import uclouvain.mssm.cp.CPGC
import uclouvain.mssm.utils.Helper

/**
  * Created by vbranders on 13/09/18.
  *
  */
object Test extends App {

  //Read the matrix
  var matrix = Helper.loadMatrix("data.txt")
  //Reorder the columns
  val colsOrder = Helper.heuristicReordering(matrix)

  println(Helper.matrixToVectorR(matrix).mkString(", "))

  val approach = new CPGC(matrix)
  approach.heuristicOnCols(colsOrder)
  val sol = approach.search()
  println(sol.getObjective())
  println("Rows: " + sol.getRowBooleanVector().mkString(", "))
  println("Cols: " + sol.getColBooleanVector().mkString(", "))

}
