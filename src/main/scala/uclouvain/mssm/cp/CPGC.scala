package uclouvain.mssm.cp

import oscar.cp._
import uclouvain.mssm.utils.{Helper, SubMatrix}

/**
  * Created by vbranders on 13/09/18.
  *
  * budget
  *
  */
class CPGC(matrix: Array[Array[Double]], budget: Int, convergence: Int, verbose: Boolean, lowerBound: Double, lightFilter: Boolean) {

  def this(matrix: Array[Array[Double]], budget: Int, convergence: Int, verbose: Boolean, lowerBound: Double) {
    this(matrix, budget, convergence, verbose, lowerBound, false)
  }

  def this(matrix: Array[Array[Double]], budget: Int, convergence: Int, verbose: Boolean) {
    this(matrix, budget, convergence, verbose, 0.0d)
  }

  def this(matrix: Array[Array[Double]], budget: Int, convergence: Int) {
    this(matrix, budget, convergence, false)
  }

  def this(matrix: Array[Array[Double]], budget: Int) {
    this(matrix, budget, 1) //none: -1, solution:0 and time: 1
  }

  def this(matrix: Array[Array[Double]]) {
    this(matrix, 10)
  }

  var completed: Boolean = false
  var interrupted: Boolean = false
  var failed: Boolean = true


  // ---------------------------------------- CP Model ----------------------------------------

  val nR: Int = matrix.size //number of rows
  val nC: Int = matrix(0).size //number of columns

  implicit val cp = CPSolver()

  val cols = Array.fill[CPBoolVar](nC)(CPBoolVar()) //index set of the columns

  val bestSolution: SubMatrix = new SubMatrix(nR, nC) //Best solution found so far

  val maxSumConstraint = new MaxSumSubMatrixConstraint(cols, matrix, lightFilter)
  maxSumConstraint.setBestBound(lowerBound)
  cp.add(maxSumConstraint)

  cp.search {
    binaryStaticIdx(cols, i => cols(i).max)
  }

  // ------------------------------ On Solution ------------------------------

  maxSumConstraint.onSolution = () => {
    //Store the new solution
    bestSolution.setBooleanRowsAndCols(maxSumConstraint.rows, (0 until nC).map(j => if(cols(j).isTrue) 1 else 0), maxSumConstraint.bestBound)
    verb(bestSolution.toString)
  }

  // ------------------------------ Search ------------------------------

  def isActiveStopCondition: Boolean = {
    if(convergence == -1) { //no convergence
      false
    } else if(convergence == 0) { //convergence on the number of solutions without improvement
      maxSumConstraint.nEvaluatedSolutions > budget
    } else if(convergence == 1) { //convergence on the time spend without improvement
      bestSolution.timeSinceImprovement() >= budget * 1000
    } else {
      true //always interrupt
    }
  }

  def heuristicOnCols(colOrder: Array[Int]): Unit = {
    val colOrderHeuristic = Array.tabulate(nC)(i => cols(colOrder(i)))
    cp.search {
      binaryStaticIdx(colOrderHeuristic, i => colOrderHeuristic(i).max)
    }
  }

  def search(LNS: Boolean, nRestarts: Int, failureLimit: Int, maxDiscrepancy: Int, LNSRestartPercentKept: Int): SubMatrix = {

    verb( "----------------------------------------\n" +
          "| Starting at: " + Helper.giveTime() + "\n" +
          "----------------------------------------")
    if(LNS){
      verb("Performing LNS (Large Neighborhood Search.")
      val stat = cp.start(failureLimit = failureLimit, maxDiscrepancy = maxDiscrepancy)
      val rand = new scala.util.Random(0)
      for(restart <- 1 to nRestarts) {
        verb("LNS restart: " + restart + "/" + budget)
        val stat = cp.startSubjectTo(failureLimit = failureLimit, maxDiscrepancy = maxDiscrepancy) {
          cp.add((0 until nR).filter(i => rand.nextInt(100) < LNSRestartPercentKept).map(i => cols(i) === bestSolution.getColIndices()(i)))
        }
      }
    } else {
      verb("Performing complete search.")
      val stat =cp.start(stopCondition = isActiveStopCondition)
    }
    interrupted = isActiveStopCondition
    completed = !isActiveStopCondition
    failed = false

    verb("Ending.")
    bestSolution
  }

  def search(): SubMatrix = {
    search(false, 0, 0, 0, 0)
  }

  def searchLNS(failureLimit: Int, nRestarts: Int, maxDiscrepancy: Int, LNSRestartPercentKept: Int): SubMatrix = {
    search(true, nRestarts, failureLimit, maxDiscrepancy, LNSRestartPercentKept)
  }

  def searchLNS(): SubMatrix = {
    searchLNS(100, 500, 2, 70)
  }

  def verb(text: String): Unit = {
    verb(text, verbose)
  }

  def verb(text: String, doPrint: Boolean): Unit = {
    if(doPrint) println("> " + text)
  }
}
