package uclouvain.mssm.cp

import oscar.algo.Inconsistency
import oscar.algo.reversible.{ReversibleDouble, ReversibleInt}
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.CPVar
import oscar.cp.{CPBoolVar, Constraint}

/**
  * Created by vbranders on 12/09/18.
  *
  */
class MaxSumSubMatrixConstraint(cols: Array[CPBoolVar], matrix: Array[Array[Double]], lightFilter: Boolean) extends Constraint(cols(0).store) {

  var onSolution: () => Unit = () => {}

  val nR = matrix.size
  val nC = matrix(0).size

  val partialSum = Array.tabulate(nR)(i => new ReversibleDouble(s, 0.0))

  //Reversible sparse-set trick to maintain unbound variables
  val unBound = Array.tabulate(nC)(i => i)
  val nUnBound = new ReversibleInt(s, nC)

  //Reversible sparse-set trick to maintain possibly interesting lines
  val candidate = Array.tabulate(nR)(i => i)
  val nCandidate = new ReversibleInt(s, nR)


  var bestBound = Double.MinValue

  var nEvaluatedSolutions = 0
  val rows: Array[Int] = Array.tabulate(nR)(i => 0) //Storing the solution's rows


  //Look-ahead filtering
  var objectiveUBNotSelect = Array.fill[Double](nC)(0.0)
  var objectiveUBForceSelect = Array.fill[Double](nC)(0)

  def setBestBound(m: Double): Unit = {
    bestBound = m
  }

  def setup(l: CPPropagStrength): Unit = {
    //Propagate method is called when any of the variables is bound
    for(i <- 0 until nC){
      cols(i).callPropagateWhenBind(this)
    }
    propagate()
  }


  override def propagate(): Unit = {
    removeBoundVariablesAndUpdatePartialSum()
    nEvaluatedSolutions += 1
    val objective = computeObjective()
    if(objective > bestBound) {
      bestBound = objective
      onSolution()
      nEvaluatedSolutions = 0
    }
    if(lightFilter) {
      pruneLight()
    } else {
      prune()
    }
  }

  def removeBoundVariablesAndUpdatePartialSum(): Unit = {
    var size = nUnBound.value
    var j = size
    while (j > 0) {
      j -= 1
      if(cols(unBound(j)).isBound) {
        size -= 1
        val tmp = unBound(j)
        if(cols(tmp).isTrue) select(tmp)
        unBound(j) = unBound(size)
        unBound(size) = tmp
      }
    }
    nUnBound.value = size
  }

  def select(c: Int): Unit = {
    var i = nCandidate.value
    while (i> 0) {
      i -= 1
      partialSum(candidate(i)) += matrix(candidate(i))(c)
    }
  }

  def computeObjective(): Double = {
    var objective = 0.0
    for(i <- 0 until nR) rows(i) = 0
    var i = nCandidate.value
    while (i > 0) {
      i -= 1
      var pSum = partialSum(candidate(i)).value
      if(pSum > 0) {
        objective += pSum
        rows(candidate(i)) = 1
      } else {
        rows(candidate(i)) = 0
      }
    }
    return objective
  }

  def pruneLight(): Unit = {
    var objectiveUB = 0.0
    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i)).value
      //Optimistic selection of positive values only for the UB
      var j = nUnBound.value
      while (j > 0) {
        j -= 1
        if(matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if(costLineUB > 0) {
        objectiveUB += costLineUB
      } else {
        //Rome this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }
    }
    nCandidate.value = nCand
    //Pruning of the branch and bound
    if(objectiveUB <= bestBound) {
      throw Inconsistency
    }
  }

  def prune(): Unit = {
    var objectiveUB = 0.0
    val nCols = nUnBound.value
    var j = nCols
    while (j > 0) {
      j -= 1
      objectiveUBNotSelect(unBound(j)) = 0
      objectiveUBForceSelect(unBound(j)) = 0
    }
    var nCand = nCandidate.value
    var i = nCand
    while (i > 0) {
      i -= 1
      var costLineUB = partialSum(candidate(i)).value
      // optimistic selection of positive values only for the UB
      j = nCols
      while (j > 0) {
        j -= 1
        if (matrix(candidate(i))(unBound(j)) > 0) {
          costLineUB += matrix(candidate(i))(unBound(j))
        }
      }
      if (costLineUB > 0) {
        objectiveUB += costLineUB
        // objectiveUBNotSelect(j) = upper bound if column j would not be selected
        j = nCols
        while (j > 0) {
          j -= 1
          if (matrix(candidate(i))(unBound(j)) > 0) {
            if (costLineUB - matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBNotSelect(unBound(j)) += costLineUB - matrix(candidate(i))(unBound(j))
            }
            objectiveUBForceSelect(unBound(j)) += costLineUB
          } else {
            objectiveUBNotSelect(unBound(j)) += costLineUB
            if (costLineUB + matrix(candidate(i))(unBound(j)) > 0) {
              objectiveUBForceSelect(unBound(j)) += costLineUB + matrix(candidate(i))(unBound(j))
            }
          }
        }
      } else {
        // remove this line as it can never become positive under this node
        nCand -= 1
        val tmp = candidate(i)
        candidate(i) = candidate(nCand)
        candidate(nCand) = tmp
      }
    }
    // Pruning of the branch and bound
    if (objectiveUB <= bestBound) {
      throw Inconsistency
    }
    j = nCols
    while (j > 0) {
      j -= 1
      if (objectiveUBNotSelect(unBound(j)) <= bestBound) {
        cols(unBound(j)).assignTrue()
      }
      if (objectiveUBForceSelect(unBound(j)) <= bestBound) {
        cols(unBound(j)).assignFalse()
      }
    }
    nCandidate.value = nCand
  }



  override def associatedVars(): Iterable[CPVar] = cols

}
