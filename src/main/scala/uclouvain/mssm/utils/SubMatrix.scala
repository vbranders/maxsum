package uclouvain.mssm.utils

/**
  * Created by vbranders on 13/09/18.
  *
  */
class SubMatrix(nRows: Int, nCols: Int) {

  protected val rows: Array[Int] = Array.tabulate[Int](nRows)(i => i)
  protected val cols: Array[Int] = Array.tabulate[Int](nCols)(j => j)
  protected var objectiveValue: Double = 0.0d
  protected var startTime: Long = System.currentTimeMillis()
  protected var lastImprovedTime: Long = System.currentTimeMillis()

  def setBooleanRowsAndCols(sRow: Array[Int], sCol: Array[Int], objective: Double): Unit = {
    for(j <- 0 until nCols) {
      cols(j) = sCol(j)
    }
    for(i <- 0 until nRows){
      rows(i) = sRow(i)
    }
    objectiveValue = objective
    lastImprovedTime = System.currentTimeMillis()
  }

  def getRowIndices(): Array[Int] = (for(i <- 0 until nRows; if rows(i) == 1) yield i).toArray

  def getColIndices(): Array[Int] = (for(j <- 0 until nCols; if cols(j) == 1) yield j).toArray

  def getRowBooleanVector(): Array[Int] = rows

  def getColBooleanVector(): Array[Int] = cols

  def getObjective(): Double = objectiveValue

  def getColsSizeSolution(): Int = cols.sum

  def getRowsSizeSolution(): Int = rows.sum

  def start(): Unit = {
    startTime = System.currentTimeMillis()
  }

  def timePassed(): Long = {
    System.currentTimeMillis() - startTime
  }

  def timeSinceImprovement(): Long = {
    System.currentTimeMillis() - lastImprovedTime
  }

  override def toString(): String = "Sum: " + getObjective() + "\t#Rows: " + getRowsSizeSolution() + "\t#Cols: " + getColsSizeSolution() + s"\tTime: ${timePassed()/1000.0}"

}
