package uclouvain.mssm.utils

import java.util.Calendar

import scala.io.Source

/**
  * Created by vbranders on 12/09/18.
  *
  */
object Helper {

  def getUpperBound(matrix: Array[Array[Double]]): Double = {
    return matrix.map(_.filter(_ > 0).sum).sum
  }

  def heuristicReordering(matrix: Array[Array[Double]]): Array[Int] = {
    val nR: Int = matrix.size //number of rows
    val nC: Int = matrix(0).size //number of columns

    val matrixTranspose = Array.tabulate(nC)(j => Array.tabulate(nR)(i => matrix(i)(j))) //transpose of the matrix
    val positiveSumColumns = Array.tabulate(nC)(j => matrixTranspose(j).filter(_>0).sum) //sum of the positive entries in columns
    val permutation = (0 until nC).sortBy(j => -positiveSumColumns(j)) //permutation based on the positive sum on columns
    return permutation.toArray
  }

  def giveTime(): String = {
    Calendar.getInstance().get(Calendar.HOUR_OF_DAY) + "h, " + Calendar.getInstance().get(Calendar.MINUTE) + "m, " + Calendar.getInstance().get(Calendar.SECOND) + "s."
  }

  def loadMatrix(filePath: String, valueSeparator: String): Array[Array[Double]] = {
    Source.fromFile(filePath).getLines().toArray.map(_.split(valueSeparator).map(_.toDouble))

  }

  def loadMatrix(filePath: String): Array[Array[Double]] = loadMatrix(filePath, "\t")

  def printMatrix(matrix: Array[Array[Double]]): Unit = {
    println(matrix.map(i => i.mkString("\t")).mkString("\n"))
  }

  /*
   * Recycle values if nrow*ncol > vector.size
   */
  def vectorToMatrix(vector: Array[Double], nrow: Int, ncol: Int): Array[Array[Double]] = {
    val mat: Array[Array[Double]] = Array.ofDim[Double](nrow, ncol)
    for(i <- 0 until nrow){
      for(j <- 0 until ncol){
        mat(i)(j) = vector((i+j*nrow)%(nrow*ncol))
      }
    }
    return mat
  }

  def matrixToVector(matrix: Array[Array[Double]]): Array[Double] = {
    Array.tabulate[Double](matrix.size * matrix(0).size)(i => matrix(i%matrix.size)(i/matrix.size))
  }

  def matrixToVectorR(matrix: Array[Array[Double]]): Array[Double] = {
    matrixToVector(matrix) ++ Array(matrix.size.toDouble, matrix(0).size.toDouble)
  }

}
